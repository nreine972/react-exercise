import {useState} from "react";

function getNavPattern(id) {
  const pattern = [
    //Level 0
    { id: `${id}`, parentId: null, title: `Business Capability ${id}` },

    //Level 2
    { id: `${id}.1`, parentId: `${id}`, title: `Business Capability ${id}.1` },
    { id: `${id}.2`, parentId: `${id}`, title: `Business Capability ${id}.2` },
    { id: `${id}.3`, parentId: `${id}`, title: `Business Capability ${id}.3` },

    //Level 3
    { id: `${id}.1.1`, parentId: `${id}.1`, title: `Business Capability ${id}.1.1` },
    { id: `${id}.1.2`, parentId: `${id}.1`, title: `Business Capability ${id}.1.2` },
    { id: `${id}.1.3`, parentId: `${id}.1`, title: `Business Capability ${id}.1.3` },
    { id: `${id}.2.1`, parentId: `${id}.2`, title: `Business Capability ${id}.2.1` },
    { id: `${id}.2.2`, parentId: `${id}.2`, title: `Business Capability ${id}.2.2` },
    { id: `${id}.2.3`, parentId: `${id}.2`, title: `Business Capability ${id}.2.3` },
    { id: `${id}.3.1`, parentId: `${id}.3`, title: `Business Capability ${id}.3.1` },
    { id: `${id}.3.2`, parentId: `${id}.3`, title: `Business Capability ${id}.3.2` },
    { id: `${id}.3.3`, parentId: `${id}.3`, title: `Business Capability ${id}.3.3` }
  ];

  return pattern;
}

function getIndexMapping(pattern) {
  const indexMapping = pattern.reduce((acc, el, i) => {
    acc[el.id] = i;
    return acc;
  }, {});

  return indexMapping;
}

function cloneArray(arr) {
  return JSON.parse(JSON.stringify(arr));
}

function getNavDataTree(pattern, indexMapping) {
  let root;
  const navDataTree = cloneArray(pattern);

  navDataTree.forEach((el) => {
    if (el.parentId === null) {
      root = el;
      return;
    }

    const parent = navDataTree[indexMapping[el.parentId]];
    parent.descendants = [...(parent.descendants || []), el];
  });

  return navDataTree;
}

export function createTreesCollection(size) {
  const collection = [];

  for (let i=1; i<=size; i++) {
    const pattern = getNavPattern(i);
    const indexMapping = getIndexMapping(pattern);
    const dataTree = getNavDataTree(pattern, indexMapping);
    collection.push(dataTree);
  }

  return collection;
}
