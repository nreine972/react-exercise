import React, {createContext, useEffect, useState} from "react";
import Filter from "./filter/Filter";
import Nav from "./nav/Nav";
import Main from "./main/Main";
import styles from "./app.module.css";

export const NavFilterContext = createContext({});

export type Application = {
  "id": string,
  "name": string,
  "spend": number,
  "BCAP1": string,
  "BCAP2": string,
  "BCAP3": string
}

function App() {
  const [applications, setApplications] = useState(null);
  const [navState, setNavState] = useState("");
  const [currentNavPath, setCurrentNavPath] = useState([]);
  const [spendFilter, setSpendFilter] = useState({
    min: 0,
    max: 0,
    value: 0
  });

  useEffect(() => {
    //Fetch the dataset from the server
    fetch("http://localhost:8080/data").then((data) => {
      data.json().then((jsonData) => {
        setApplications(jsonData)

        //Sort the spending values and extract the min and max
        const spendValues = jsonData.map((app:Application) => app.spend);
        spendValues.sort((a:number, b:number) => a - b);
        const min = spendValues[0];
        const max = spendValues[spendValues.length-1];
        setSpendFilter({
          min,
          max,
          value: min
        })
      });
    });
  }, []);

  return (
    <NavFilterContext.Provider value={{setNavState, navState, currentNavPath, setCurrentNavPath, spendFilter}}>
      <div className={styles.container}>
        <h1 className={styles.title}>Pharos Coding Exercise</h1>
        <div className={styles.nav}>
          <Nav />
          <Filter
            value={spendFilter.value}
            min={spendFilter.min}
            max={spendFilter.max}
            updateHandler={setSpendFilter}
          />
        </div>
        <div className={styles.main}>
          <Main applications={applications} />
        </div>
      </div>
    </NavFilterContext.Provider>
  );
}

export default App;