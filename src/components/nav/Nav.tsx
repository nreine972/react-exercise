import React, {useState, useEffect} from "react";
import NavTree from "../nav-tree/NavTree";
import {createTreesCollection} from "../../tools";
import styles from "./nav.module.css";

export default function Nav() {
  const [treesCollection] = useState(createTreesCollection(3));

  return (
    <div className={styles.container}>
      {treesCollection && treesCollection.map((tree, index) =>
        <NavTree key={`nt-${index}`} navDataTree={tree} />
      )}
    </div>
  )
}
