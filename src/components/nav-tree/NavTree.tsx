import React, {useEffect, useState} from "react";
import NavNode, {NavNodeType} from "../nav-node/NavNode";
import styles from "./nav-tree.module.css";

type NavDataTreeProps = {
  navDataTree: NavNodeType[]
}

function NavTree({navDataTree}:NavDataTreeProps) {
  const [root] = useState<NavNodeType|undefined>(
    navDataTree.find(element => element.parentId === null)
  );

  return(
    <div className={styles.container}>
      {root && (
        <ul>
          <NavNode id={root.id} title={root.title} descendants={root.descendants} />
        </ul>
      )}
    </div>
  );
}

export default NavTree;