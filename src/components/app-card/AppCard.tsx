import React from "react";
import styles from "./app-card.module.css";
import {Application} from "../App";

type AppCardProps = {
  data: Application,
  enabled: boolean
}

function AppCard({data, enabled}:AppCardProps) {
  return (
    <>
      {enabled && (
        <div className={styles.container}>
          <div className={styles.wrapper}>
            <h4>{data.name}</h4>
            <p>Total spend: {`$${data.spend}`}</p>
          </div>
        </div>
      )}
    </>
  )
}

export default AppCard;