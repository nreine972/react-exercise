import React, {useEffect} from "react";
import styles from "./filter.module.css";
import {NavNodeType} from "../nav-node/NavNode";

type FilterProps = {
  min:number,
  max:number,
  value:number,
  updateHandler: Function
}

function Filter({min, max, value, updateHandler}:FilterProps) {

  // @ts-ignore
  const onChangeHandler = function (e) {
    updateHandler({
      min,
      max,
      value : e.target.value
    })
  }

  return(
    <div className={styles.container}>
      <h3 className={styles.title}>Filter</h3>
      <label className={styles.label}>Spending</label>
      <input className={styles.input} type="range" onChange={onChangeHandler} min={min} max={max} />
      <div className={styles.labelGroup}>
        <span className={styles.min}>{min}</span>
        <span className={styles.max}>{max}</span>
      </div>
    </div>
  );
}

export default Filter;