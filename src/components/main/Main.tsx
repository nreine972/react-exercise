import React, {useContext} from "react";
import styles from "./main.module.css";
import AppCard from "../app-card/AppCard";
import {Application, NavFilterContext} from "../App";

type MainPropsType = {
  applications: Application[] | null
}

function Main({applications}:MainPropsType) {

  // @ts-ignore
  const {navState, spendFilter} = useContext(NavFilterContext);

  //Assuming that a card should be displayed if :
  // - its spend value is less or equal to the current filter spending value
  // - Either BCAP1, or BCAP2, or BCAP3 matches the selected nav button
  const setEnabled = (app:Application) => {
    let enabled = false;
    if([app.BCAP1, app.BCAP2, app.BCAP3].indexOf(navState) !== -1 || navState === "") {
      if(app.spend <= spendFilter.value){
        enabled = true;
      }
    }
    else {
      enabled = false;
    }
    return enabled;
  }

  return (
    <div className={styles.container}>
      {applications && applications.map((app) =>
        <AppCard key={app.id} data={app} enabled={setEnabled(app)} />
      )}
    </div>
  )
}

export default Main;