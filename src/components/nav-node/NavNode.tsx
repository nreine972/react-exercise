import React, {useContext, useEffect, useState} from "react";
import styles from "./nav-node.module.css";
import {NavFilterContext} from "../App";

export type NavNodeType = {
  id?: number,
  parentId?: number,
  title: string,
  descendants: NavNodeType[],
  activePath?: boolean
}

function NavNode({title, descendants, id}:NavNodeType) {
  // @ts-ignore
  const {setNavState, navState, currentNavPath, setCurrentNavPath} = useContext(NavFilterContext);

  const onClickHandler = (e:object) => {
    setNavState(title);
    setCurrentNavPath([id]);
  }

  useEffect(()=>{
    if(descendants) {
      const childIds = descendants.map(d => d.id);

      let searchIndex = -1;
      for(let i=0; i<currentNavPath.length; i++){
        searchIndex = childIds.indexOf(currentNavPath[i]);
      }

      if(searchIndex !== -1 && currentNavPath.indexOf(id) === -1){
        setCurrentNavPath([...currentNavPath, id]);
      }
    }

  }, [currentNavPath])

  return (
    <li className={styles.container}>
      <button
        className={navState === title ? styles.enabled : styles.disabled}
        onClick={onClickHandler}>
        {title}
      </button>
      {descendants && (
        <div className={styles.submenu}>
          <ul className={currentNavPath.indexOf(id) === -1 ? styles.inactive : styles.active}>
            {descendants.map(
              d => <NavNode
                key={d.id}
                id={d.id}
                title={d.title}
                descendants={d.descendants}
              />
            )}
          </ul>
        </div>
      )}
    </li>
  )
}

export default NavNode;